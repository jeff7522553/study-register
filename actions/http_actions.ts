import { Observable } from 'rxjs/Observable';
import '../../share/rxjs-operators'

var request = require("request");

// request.debug = true //log 截取器

function getAction(options) {
    return Observable.create(
        observer => {
            request(options, (error, response, body) => {
                if (!response || error) {
                    observer.error({ statusCode: 400, msg: `GET ${options['url']} error`, info: error || JSON.parse(body) });
                } else {
                    if (response.statusCode >= 400) observer.error({ statusCode: response.statusCode, msg: "GET error", info: error || body || "connect error" });

                    if (response.statusCode >= 200 && response.statusCode < 300)
                        observer.next({ statusCode: response.statusCode, msg: "GET Success", info: body ? JSON.parse(body) : "ok" })
                    // observer.next(JSON.parse(body)); 
                    else observer.error({ msg: `GET ${options['url']} error`, info: "connect error" })
                }
            })
        })
}

function postAction(options) {
    return Observable.create(
        observer => {
            request(options, (error, response, body) => {
                if (!response || error) {
                    observer.error({ statusCode: 404, msg: `POST ${options['url']} error`, info: error || JSON.parse(body) });
                } else {
                    if (response.statusCode >= 200 && response.statusCode < 300) observer.next(
                        { statusCode: response.statusCode, msg: "POST Success", info: body ? JSON.parse(body) : "ok" }
                    );
                    else observer.error({ statusCode: response.statusCode, msg: `POST ${options['url']} error`, info: "connect error" })
                }
            })
        })
}

function deleteAction(options) {
    return Observable.create(
        observer => {
            request(options, (error, response, body) => {
                if (!response || error) {
                    observer.error({ statusCode: 400, msg: `DELETE ${options['url']} error`, info: error || JSON.parse(body) });
                } else {
                    if (response.statusCode >= 400) observer.error({ statusCode: response.statusCode, msg: "DELETE error", info: error || body || "connect error" });

                    if (response.statusCode >= 200 && response.statusCode < 300)
                        observer.next({ statusCode: response.statusCode, msg: "DELETE Success", info: body ? JSON.parse(body) : "ok" })
                    // observer.next(JSON.parse(body)); 
                    else observer.error({ msg: `DELETE ${options['url']} error`, info: "connect error" })
                }
            })
        })
}



module.exports = { getAction, postAction, deleteAction }