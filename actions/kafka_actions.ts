import * as kafka from "kafka-node";
import { Observable, BehaviorSubject } from "rxjs";
import { share, tap, retryWhen, map, filter, catchError, mapTo } from "rxjs/operators";

import * as fs from "fs";
import { bindNodeCallback } from "rxjs/observable/bindNodeCallback";
import { from } from "rxjs/observable/from";
import { fromEvent } from "rxjs/observable/fromEvent";
import { merge } from "rxjs/observable/merge";

import { mergeMap } from "rxjs/operators";
import { of } from "rxjs/observable/of";


export function listen(clientHost: string, serverId, topicName: string[]) {
    // const client = new kafka.KafkaClient({ kafkaHost: clientHost });
    // let client = new kafka.Client(clientHost);
    console.log('serverId ',serverId)
    return consumer(clientHost, serverId, topicName)
}






function consumer(client: string, serverId: string, topicName: string[]) {
  console.log('topicName ',topicName)
  console.log('serverId ',serverId)
    // let readFileCallBack$ = readOffsetLogFileCallBack(`${process.cwd()}/node_services/logs/kafka-offect-log.txt`);
    let options = {
        kafkaHost: client,
        groupId: serverId,
        fromOffset: "latest"
    };

    let consumerGroup = new kafka.ConsumerGroup(options, topicName);
    return of(consumerGroup).pipe(
        mergeMap((consumer: any) => {
            return merge(
                fromEvent(consumer, 'message').map(msg => {

                    return { type: 1, content: msg }
                }),
                fromEvent(consumer, 'error').map(msg => ({ type: 0, content: msg }))
            )
        })
    )

}

function readOffsetLogFileCallBack(file) {
    const readFileAsObservable = bindNodeCallback(fs.readFile);
    const readFileCallBack$ = readFileAsObservable(file);
    return readFileCallBack$
}

function writeOffsetLogFile(offset): void {
    fs.writeFile(`${process.cwd()}/node_services/logs/kafka-offect-log.txt`, offset, err => {
        if (err)
            console.log("writeOffsetLogFile err ", err);
        else
            console.log('Write operation complete.');
    });
}

