
import * as socketIO from "socket.io";
import { Message } from "../../share/interface/real-time-interface";


let socketListener_mapList = {};

let globalIo: socketIO;

export function connection(server) {
    if (!globalIo) {
        let io = socketIO(server);

        globalIo = io;
        globalIo.on('connection',socket=>{
            socket.on('disconnect',data=>{
                console.log('disconnect====> ',socket['id'])
            });

            socket.on('connect',data=>{
                console.log('connect====> ',data)
            })
        })

    }
}

export function pushAll(topicName,msg: Message) {

    if (globalIo)
        globalIo.sockets.emit(topicName, msg)
    else
        throw new console.error('socket not connection');
} 