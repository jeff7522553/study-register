const express = require('express');
const router = express.Router();

const axios = require('axios');
const API = 'http://127.0.0.1:8000/api';

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});


router.get('/store', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB
  console.log("get store!!!")
  axios.get(`${API}/store`)
    .then(posts => {
      console.log("get posts!!!",posts.data)
      res.status(200).json(posts.data);
    })
    .catch(error => {
      console.log("get error!!!", error)
      res.status(500).send(error)
    });
});



module.exports = router;
