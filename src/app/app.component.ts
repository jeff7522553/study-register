import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { State } from "./reducers";
import { GetSerialNumber } from './actions/serial-number.actions';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "study-register";

  subscriber = {
    getSerialNumber$: undefined
  };

  ngOnInit(){
    if(this.swUpdate.isEnabled){
      this.swUpdate.available.subscribe(()=>{
        if(confirm("有最新版本，是否更新？")){
          window.location.reload();
        }
      });
    }
  }

  constructor(private store: Store<State>, private swUpdate: SwUpdate) {
    console.log("getSerialNumber");
    this.store.dispatch(new GetSerialNumber({}));
    this.subscriber["getSerialNumber$"] = store.subscribe(x => {
      console.log(x);
    });
  }
}
