import { Action } from "@ngrx/store";
import { ClassActions, ClassActionTypes, AddClassListA, AddClassListB } from '../actions/class.actions';

export interface ClassState {
  ClassListA: Array<String>;
  ClassListB: Array<String>;
  SelectedClassListA: Array<String>;
  SelectedClassListB: Array<String>;
}

export const initialState: ClassState = {
  ClassListA: [],
  ClassListB: [],
  SelectedClassListA: [],
  SelectedClassListB: []
};

export function classReducer(
  state = initialState,
  action: ClassActions
): ClassState {
  switch (action.type) {
    case ClassActionTypes.GetClassListA:
      return { ...state, ClassListA: action.payload["ClassListA"] };
    case ClassActionTypes.GetClassListB:
      return { ...state, ClassListB: action.payload["ClassListB"] };

      case ClassActionTypes.AddClassListA:
      return { ...state, ClassListA: [...state.ClassListA, action.payload["ClassListA"]] };
    case ClassActionTypes.AddClassListB:
      return { ...state, ClassListB: [...state.ClassListB, action.payload["ClassListB"]] };

      case ClassActionTypes.RemoveClassListA:
      return { ...state, ClassListA: [...state.ClassListA.filter(item => item !== action.payload.class)] };
    case ClassActionTypes.RemoveClassListB:
      return { ...state, ClassListB: [...state.ClassListB.filter(item => item !== action.payload.class)] };


    default:
      return state;
  }
}
