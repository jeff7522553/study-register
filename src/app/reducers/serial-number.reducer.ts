import { Action } from "@ngrx/store";
import {
  SerialNumberActions,
  SerialNumberActionTypes,
} from "../actions/serial-number.actions";

export interface SerialNumberState {
  serialNumber: string;
}

export const initialState: SerialNumberState = {
  serialNumber: null
};

export function serialNumberReducer(
  state = initialState,
  action: SerialNumberActions
): SerialNumberState {
  switch (action.type) {
    case SerialNumberActionTypes.GetSerialNumber:
      // console.log("GetSerialNumber state", state);
      return { ...state };

    case SerialNumberActionTypes.GetSerialNumberSuccess:
      return { ...state, serialNumber: action['payload'] };

    default:
      return state;
  }
}
