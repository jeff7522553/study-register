import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from "@ngrx/store";
import { environment } from "../../environments/environment";
import { InformationState, informationReducer } from "./information.reducer";
import { ClassState, classReducer } from "./class.reducer";
import { SerialNumberState, serialNumberReducer } from './serial-number.reducer';
import { StatusReducer, StatusState } from './status.reducer';

export interface State {
  status: StatusState,
  serialNumber: SerialNumberState,
  information: InformationState;
  class: ClassState;
}

export const reducers: ActionReducerMap<State> = {
  status: StatusReducer,
  serialNumber: serialNumberReducer,
  information: informationReducer,
  class: classReducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];
