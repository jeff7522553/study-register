import { Action } from '@ngrx/store';
import { InformationActionTypes, PostInformations, InformationActions } from '../actions/information.actions';


export interface InformationState {
  informations: Array<String>
}

export const initialState: InformationState = {
  informations: [],
};

export function informationReducer(state = initialState, action: InformationActions): InformationState {
  switch (action.type) {
    case InformationActionTypes.PostInformations:
      return {...state, informations: [...state.informations, action.payload['informations']]};
    default:
      return state;
  }
}
