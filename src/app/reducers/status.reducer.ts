import { Action } from "@ngrx/store";
import {  SerialNumberActionTypes } from "../actions/serial-number.actions";
import { InformationActionTypes } from '../actions/information.actions';
import { ClassActionTypes, AddClassListB } from '../actions/class.actions';

export interface StatusState {
  status: string;
  statusLog: Array<string>;
}

export const initialState: StatusState = {
  status: null,
  statusLog: []
};

export function StatusReducer(
  state = initialState,
  action: Action
): StatusState {
  switch (action.type) {
    case SerialNumberActionTypes.GetSerialNumber:
      return {
        ...state,
        status: SerialNumberActionTypes.GetSerialNumber,
        statusLog: [...state.statusLog, SerialNumberActionTypes.GetSerialNumber]
      };

    case SerialNumberActionTypes.GetSerialNumberSuccess:
      return {
        ...state,
        status: SerialNumberActionTypes.GetSerialNumberSuccess,
        statusLog: [
          ...state.statusLog,
          SerialNumberActionTypes.GetSerialNumberSuccess
        ]
      };
      case SerialNumberActionTypes.GetSerialNumber:
      return {
        ...state,
        status: SerialNumberActionTypes.GetSerialNumber,
        statusLog: [...state.statusLog, SerialNumberActionTypes.GetSerialNumber]
      };

    case InformationActionTypes.PostInformations:
      return {
        ...state,
        status: InformationActionTypes.PostInformations,
        statusLog: [
          ...state.statusLog,
          InformationActionTypes.PostInformations
        ]
      };

      case ClassActionTypes.AddClassListA:
      return {
        ...state,
        status: ClassActionTypes.AddClassListA,
        statusLog: [
          ...state.statusLog,
          ClassActionTypes.AddClassListA
        ]
      };

      case ClassActionTypes.AddClassListB:
      return {
        ...state,
        status: ClassActionTypes.AddClassListB,
        statusLog: [
          ...state.statusLog,
          ClassActionTypes.AddClassListB
        ]
      };

      case ClassActionTypes.RemoveClassListA:
      return {
        ...state,
        status: ClassActionTypes.RemoveClassListA,
        statusLog: [
          ...state.statusLog,
          ClassActionTypes.RemoveClassListA
        ]
      };

      case ClassActionTypes.RemoveClassListB:
      return {
        ...state,
        status: ClassActionTypes.RemoveClassListB,
        statusLog: [
          ...state.statusLog,
          ClassActionTypes.RemoveClassListB
        ]
      };

      case ClassActionTypes.GetClassListA:
      return {
        ...state,
        status: ClassActionTypes.GetClassListA,
        statusLog: [
          ...state.statusLog,
          ClassActionTypes.GetClassListA
        ]
      };


      case ClassActionTypes.GetClassListB:
      return {
        ...state,
        status: ClassActionTypes.GetClassListB,
        statusLog: [
          ...state.statusLog,
          ClassActionTypes.GetClassListB
        ]
      };

    default:
      return state;
  }
}
