import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { InformationEffects } from './information.effects';

describe('InformationEffects', () => {
  let actions$: Observable<any>;
  let effects: InformationEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        InformationEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(InformationEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
