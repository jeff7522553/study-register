import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { SerialNumberEffects } from './serial-number.effects';

describe('SerialNumberEffects', () => {
  let actions$: Observable<any>;
  let effects: SerialNumberEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SerialNumberEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(SerialNumberEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
