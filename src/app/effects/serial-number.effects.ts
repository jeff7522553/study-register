import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { SerialNumberActions, SerialNumberActionTypes, GetSerialNumberSuccess, GetSerialNumberFail, GetSerialNumber } from '../actions/serial-number.actions';
import { ServiceService } from '../service.service';
import { mergeMap, catchError, map } from 'rxjs/operators';





@Injectable()
export class SerialNumberEffects {
  constructor(private actions$: Actions, private httpService: ServiceService) {}


  @Effect()
  getSerialNumber$: Observable<any> = this.actions$.pipe(
    ofType(SerialNumberActionTypes.GetSerialNumber),
    mergeMap( action =>
      this.httpService.getSerialNumber(action["payload"]).pipe(
        map(data => new GetSerialNumberSuccess(data)),
        catchError(err => of(new GetSerialNumberFail(err)))
      )
      )
  )


}
