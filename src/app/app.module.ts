import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { HomeComponent } from './home/home.component';
import { ContentComponent } from './content/content.component';
import { InformationComponent } from './content/information/information.component';
import { ClassComponent } from './content/class/class.component';
import { ConfirnComponent } from './content/confirn/confirn.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material';
import { EffectsModule } from '@ngrx/effects';
import { SerialNumberEffects } from './effects/serial-number.effects';
import { InformationEffects } from './effects/information.effects';
import { ClassEffects } from './effects/class.effects';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContentComponent,
    // InformationComponent,
    // ClassComponent,
    // ConfirnComponent
  ],
  imports: [
    FormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, { metaReducers,}),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    MatButtonModule,
    EffectsModule.forRoot([SerialNumberEffects, InformationEffects, ClassEffects]),
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
