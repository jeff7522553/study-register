import { Action } from '@ngrx/store';

export enum SerialNumberActionTypes {
  GetSerialNumber = '[SerialNumber] Get SerialNumber',
  GetSerialNumberSuccess = '[SerialNumber] Get SerialNumber Success',
  GetSerialNumberFail = '[SerialNumber] Get SerialNumber Fail',
}

export class GetSerialNumber implements Action {
  readonly type = SerialNumberActionTypes.GetSerialNumber;
  constructor(public payload: any) {};
}

export class GetSerialNumberSuccess implements Action {
  readonly type = SerialNumberActionTypes.GetSerialNumberSuccess;
  constructor(public payload: any) {};
}
export class GetSerialNumberFail implements Action {
  readonly type = SerialNumberActionTypes.GetSerialNumberFail;
  constructor(public payload: any) {};
}

export type SerialNumberActions = GetSerialNumber|
GetSerialNumberSuccess|
GetSerialNumberFail;
