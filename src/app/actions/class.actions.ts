import { Action } from '@ngrx/store';

export enum ClassActionTypes {
  GetClassListA    = '[Class] Get ClassListA',
  AddClassListA    = '[Class] Add ClassListA',
  RemoveClassListA = '[Class] Remove ClassListA',

  GetClassListB    = '[Class] Get ClassListB',
  AddClassListB    = '[Class] Add ClassListB',
  RemoveClassListB = '[Class] Remove ClassListB',
}

export class GetClassListA implements Action {
  readonly type = ClassActionTypes.GetClassListA;
  constructor(public payload: any){}
}

export class AddClassListA implements Action {
  readonly type = ClassActionTypes.AddClassListA;
  constructor(public payload: any){}
}

export class RemoveClassListA implements Action {
  readonly type = ClassActionTypes.RemoveClassListA;
  constructor(public payload: any){}
}

export class GetClassListB implements Action {
  readonly type = ClassActionTypes.GetClassListB;
  constructor(public payload: any){}
}

export class AddClassListB implements Action {
  readonly type = ClassActionTypes.AddClassListB;
  constructor(public payload: any){}
}

export class RemoveClassListB implements Action {
  readonly type = ClassActionTypes.RemoveClassListB;
  constructor(public payload: any){}
}


export type ClassActions = GetClassListA|
AddClassListA|
RemoveClassListA|
GetClassListB|
AddClassListB|
RemoveClassListB;
