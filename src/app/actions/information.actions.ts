import { Action } from '@ngrx/store';

export enum InformationActionTypes {
  PostInformations = '[Information] Post Informations',
}

export class PostInformations implements Action {
  readonly type = InformationActionTypes.PostInformations;
  constructor(public payload: any) {}
}


export type InformationActions = PostInformations;
