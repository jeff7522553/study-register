import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  private API_URL  =  'http://localhost:3000';

  constructor(private http: HttpClient) {}

  getSerialNumber(term) {
    console.log("getSerialNumber  term", term);
    // const options = term ?
    // { params: new HttpParams().set('degree', term.payload['degree']).set('examIdx', term.payload['examIdx']) } : {};
    // return this.http.get(`${this.API_URL}/api/store`,  options);
    return this.http.get(`${this.API_URL}/api/store`);
  }


}
