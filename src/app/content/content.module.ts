import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClassComponent } from './class/class.component';
import { InformationComponent } from './information/information.component';
import { ConfirnComponent } from './confirn/confirn.component';

import { RouterModule, Routes } from '@angular/router';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatButtonModule,
  MatIconModule,
  MatCardModule,
  MatFormFieldModule,
  MatSelectModule,
  MatOptionModule,
  MatInputModule,
  MatRadioModule,
  MatExpansionModule

} from '@angular/material';
import { EffectsModule } from '@ngrx/effects';
import { SerialNumberEffects } from '../effects/serial-number.effects';
import { InformationEffects } from '../effects/information.effects';
import { ClassEffects } from '../effects/class.effects';

// Routes
const contentRoutes: Routes = [
  { path: 'information', component: InformationComponent },
  { path: 'class', component: ClassComponent },
  { path: 'confirn', component: ConfirnComponent }
];
@NgModule({
  declarations: [InformationComponent, ClassComponent, ConfirnComponent],
  imports: [
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(contentRoutes),
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatInputModule,
    MatRadioModule,
    MatExpansionModule,
    // EffectsModule.forRoot([SerialNumberEffects, InformationEffects, ClassEffects]),
  ]
})
export class ContentModule {}
