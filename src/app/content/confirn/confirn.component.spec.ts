import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirnComponent } from './confirn.component';

describe('ConfirnComponent', () => {
  let component: ConfirnComponent;
  let fixture: ComponentFixture<ConfirnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
