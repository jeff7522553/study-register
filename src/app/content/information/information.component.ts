import { Component, OnInit, OnDestroy, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";
import { filter, map } from "rxjs/operators";
import * as moment from "moment";
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from "@angular/material/core";
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter
} from "@angular/material-moment-adapter";
import { State } from "src/app/reducers";

const MY_DATE_FORMATS = {
  parse: {
    dateInput: "YYYY/MM/DD"
  },
  display: {
    dateInput: "YYYY/MM/DD",
    monthYearLabel: "MMMM YYYY",
    dateA11yLabel: "YYYY/MM/DD",
    monthYearA11yLabel: "MMMM YYYY"
  }
};
@Component({
  selector: "app-information",
  templateUrl: "./information.component.html",
  styleUrls: ["./information.component.css"],
  providers: [
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    { provide: MAT_DATE_LOCALE, useValue: "zh-TW" },

    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ]
})
export class InformationComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {}

  stepIdx : number = 1;
  infoForm1: FormGroup;
  infoForm2: FormGroup;
  infoForm3: FormGroup;
  infoForm4: FormGroup;

  constructor(
    private _formbuilder: FormBuilder,
    private store: Store<State>,
    elem: ElementRef
  ) {
    this.initCompanyForm()
  }

  initCompanyForm() {
    this.infoForm1 = this._formbuilder.group({
      name: [undefined, Validators.required],
      idCardNum: [undefined],
      gender: [undefined],
      birthDay: [undefined],
      email: [undefined],
      phone: [undefined],
      schoolName: [undefined],
      studyYear: [undefined],
      studyClassName: [undefined],
    });

    this.infoForm2 = this._formbuilder.group({
      name: [undefined, Validators.required],
      phone: [undefined],
      homePhone: [undefined],
      email: [undefined],
      city: [undefined],
      town: [undefined],
      street: [undefined],
    });

    this.infoForm3 = this._formbuilder.group({
      sibling: [undefined,],
      name1: [undefined],
      schoolName1: [undefined],
      studyYear1: [undefined],
      studyClassName1: [undefined],
      name2: [undefined],
      schoolName2: [undefined],
      studyYear2: [undefined],
      studyClassName2: [undefined],
    });


    this.infoForm4 = this._formbuilder.group({
      introducer: [undefined,],

    });

    console.log(this.infoForm1.value, "infoForm1");
    // this.infoForm1.disable();
  }

  goBackCard(){
    this.stepIdx -= 1
  }
  goNextCard(){
    this.stepIdx += 1
  }
  // subscriber = {
  //   selectedGroupData: undefined,
  //   closeDrawer: undefined
  // };
  // constructor(private _formbuilder: FormBuilder, private store: Store<{ state: State }>, elem: ElementRef) {
  //   let tagertComponent = elem.nativeElement.tagName.toLowerCase();
  //   this.initCompanyForm();



  ngOnInit() {}



  // submitCompanyForm() {
  // // console.log('fffff ', this.companyForm)
  //   const effectData = {
  //     address: this.companyForm.value['address'],
  //     annualLeaveType: this.companyForm.value['annualLeaveType'],
  //     contactEmail: this.companyForm.value['contactEmail'],
  //     contactName: this.companyForm.value['contactName'],
  //     contactPhone: this.companyForm.value['contactPhone'],
  //     expireDate: this.companyForm.value['expireDate'] ? Number(this.companyForm.value['expireDate'].format('x')) : null,
  //     fax: this.companyForm.value['fax'],
  //     // groupId: this.companyForm.value['id'], // required
  //     id: this.companyForm.value['id'], // required
  //     laborInsuranceType: this.companyForm.value['laborInsuranceType'],
  //     laborInsuranceTypeCode: this.companyForm.value['laborInsuranceTypeCode'],
  //     level: this.companyForm.value['level'],
  //     name: this.companyForm.value['name'],
  //     phone: this.companyForm.value['phone'],
  //     responsibleEmail: this.companyForm.value['responsibleEmail'],
  //     responsibleName: this.companyForm.value['responsibleName'],
  //     responsiblePhone: this.companyForm.value['responsiblePhone'],
  //     setupDate: this.companyForm.value['setupDate'] ? Number(this.companyForm.value['setupDate'].format('x')) : null,
  //     state: this.companyForm.value['state'],
  //     taxId: this.companyForm.value['taxId'],
  //   };

  // // console.log("submitCompanyForm   effectData", effectData)
  //   this.store.dispatch(new PutGroupEffect(effectData));
  //   this.companyForm.disable();
  //   // this.store.dispatch(new CloseSideContent);
  // }

  openedDatePicker(option?: {}) {
    option['pickerone'].opened = true
  }

}
